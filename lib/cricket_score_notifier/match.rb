class Match

  @matchInfo = []
  @bowling = []
  @batting = []
  @comms = []

  def initialize(json)
    @matchInfo = json
    @bowling = json['bowling']
    @batting = json['batting']
    @comms = json['comms']
  end

  def is_running?
    return (@matchInfo['isStarted'] == '1') ? true : false
  end

  def get_summary
    return @matchInfo['current_summary']
  end

  def get_score
    @matchInfo['team'].each do | team |
      if team["live_current"] == '1'
        return team['score']
      end
    end

    return nil
  end

  def last_ball
    if @comms.size == 0
      return false
    end

    last_comm = @comms[0] # get the commentary for the last ball

    # make sure 'text' key is present and clean
    unless last_comm['text'].nil?
      last_comm['text'] = _clean(last_comm['text'])
    else
      last_comm['text'] = ''
    end

    # pre_text is not present, so we need to set it
    unless last_comm['pre_text'].nil?
      last_comm['pre_text'] = _clean(last_comm['pre_text'])
    else
      last_comm['pre_text'] = ''
    end

    if last_comm['dismissal'].nil?
      last_comm['dismissal'] = ''
    end

    return last_comm
  end

  def last_runs
    lb = last_ball()
    runs = 0

    unless lb and lb['event']
      return 0
    end

    if lb['event'] != 'no run'
      matches = lb['event'].match(/(\d{1,}|four|six)/i)
      if matches
        runs = _str_to_num(matches[1]).to_i
      end
    end

    return runs
  end

  private

  # Convert a string to integer
  def _str_to_num(s)
    if s.match(/four/i)
      return 4
    elsif s.match(/five/i)
      return 5
    elsif s.match(/six/i)
      return 6
    end

    return s
  end

  # Cleanup the string for display
  def _clean(text)
    return text.gsub(/<\/?[^>]*>/, "").strip()
  end
end